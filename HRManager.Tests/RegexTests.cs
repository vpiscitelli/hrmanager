﻿using System.Text.RegularExpressions;
using NUnit.Framework;

namespace HRManager.Tests
{
    [TestFixture]
    public class RegexTests
    {
        private string postal1;
        [SetUp]
        protected void SetUp()
        {
            postal1 = "K2E 7H4";
        }

        [Test]
        public void PostalCode_Success()
        {
            var pattern = @"^[ABCEGHJ-NPRSTVXY]\d[ABCEGHJ-NPRSTV-Z][ ]\d[ABCEGHJ-NPRSTV-Z]\d$";
            var regex = new Regex(pattern);
            Assert.IsTrue(regex.Match(postal1).Success);
        }
    }
}
