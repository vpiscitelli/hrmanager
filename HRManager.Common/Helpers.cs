﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace HRManager.Common
{
    public static class EnumHelpers
    {
        public static string GetDisplayName(this Enum enumValue)
        {
            var enumType = enumValue.GetType();

            return enumType
                       .GetMember(enumValue.ToString())
                       .Where(x => x.MemberType == MemberTypes.Field && ((FieldInfo)x).FieldType == enumType)
                       .First()
                       .GetCustomAttribute<DisplayAttribute>()?.Name ?? enumValue.ToString();
        }
    }
}
