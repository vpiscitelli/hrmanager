﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using HRManager.Data.Models;

namespace HRManager.Web.Models
{
    public class EmployeeViewModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "First Name")]
        [MaxLength(100)]
        public string FirstName { get; set; }

        [Display(Name = "Initial")]
        [MaxLength(5)]
        public string Initial { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        [MaxLength(100)]
        public string LastName { get; set; }

        [Display(Name = "Address")]
        [MaxLength(100)]
        public string AddressLine1 { get; set; }

        [Display(Name = "Address 2")]
        [MaxLength(100)]
        public string AddressLine2 { get; set; }

        [Display(Name = "City")]
        [MaxLength(100)]
        public string City { get; set; }

        [Display(Name = "Province")]
        [MaxLength(50)]
        public string Province { get; set; }

        [Display(Name = "Postal Code")]
        [MaxLength(7)]
        [MinLength(7)]
        [RegularExpression(@"^[ABCEGHJ-NPRSTVXY]\d[ABCEGHJ-NPRSTV-Z][ ]\d[ABCEGHJ-NPRSTV-Z]\d$", 
            ErrorMessage = " Postal Code must be in the form ‘ANA NAN’, where ‘A’ represents an alphabetic character and ‘N’ represents a numeric character (e.g., K1A 0T6)")]
        public string PostalCode { get; set; }

        [Display(Name = "Home Phone")]
        [Phone]
        public string HomePhone { get; set; }

        [Display(Name = "Work Phone")]
        [MaxLength(30)]
        [Phone]
        public string WorkPhone { get; set; }

        [Display(Name = "Cell Phone")]
        [MaxLength(30)]
        [Phone]
        public string CellPhone { get; set; }

        [Required]
        [Display(Name = "E-mail")]
        [MaxLength(100)]
        [EmailAddress]
        public string EmailAddress { get; set; }


        [Required]
        [Display(Name = "Date Joined")]
        [DataType(DataType.Date)]
        public DateTime DateJoined { get; set; }

        [Required]
        [Display(Name = "Position")]
        [MaxLength(100)]
        public string Position { get; set; }

        [Display(Name = "Reports To")]
        public int ReportsToId { get; set; }

        public Employee ReportsTo { get; set; }

        public string FullName => $"{FirstName} {LastName}";
        public string FullNameWithInitial => $"{FirstName} {Initial} {LastName}";
    }
}