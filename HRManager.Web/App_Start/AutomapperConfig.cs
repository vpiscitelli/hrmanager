﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using HRManager.Data.Models;
using HRManager.Web.Models;

namespace HRManager.Web.App_Start
{
    public class AutomapperConfig
    {
        public MapperConfiguration CreateConfiguration()
        {
            var config = new MapperConfiguration(cfg =>
            {
                // Add all profiles in current assembly
                cfg.AddProfile<HRManagerProfile>();
            });

            return config;
        }
    }

    public class HRManagerProfile : Profile
    {
        public HRManagerProfile()
        {
            CreateMap<EmployeeViewModel, Employee>().ReverseMap();
        }
    }
}