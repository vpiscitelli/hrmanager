﻿function InitSelect2(selector) {
    $(selector).select2({
        ajax: {
            url: '/api/Employees',
            dataType: 'json',
            processResults: function (data) {
                return {
                    results: $.map(data,
                        function (item) {
                            return {
                                text: item.FirstName + " " + item.LastName,
                                id: item.Id,
                                position: item.Position
                            }
                        })
                };
            },
            cache: true
        },
        placeholder: 'Please choose someone to report to',
        minimumResultsForSearch: Infinity,
        templateResult: formatEmployee,
        templateSelection: formatEmployeeSelection
    });
}

function formatEmployee(item) {
    console.log(item);
    if (item.loading) {
        return item.text;
    }

    return $('<div class="row">' +
        '<div class="col-md-5">' +
        item.text +
        '</div>' +
        '<div class="col-md-7">' +
        item.position +
        '</div>' +
        '</div>');
}

function formatEmployeeSelection(item) {
    return item.text;
}