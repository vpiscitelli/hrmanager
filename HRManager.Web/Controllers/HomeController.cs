﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRManager.Data.Services;

namespace HRManager.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IEmployeeData employeeData;

        public HomeController(IEmployeeData employeeData)
        {
            this.employeeData = employeeData;
        }

        public ActionResult Index()
        {
            var model = employeeData.GetAll();
            return View(model);
        }

        public ActionResult About()
        {
            return View();
        }
    }
}