﻿using HRManager.Data.Models;
using HRManager.Data.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using HRManager.Web.Models;

namespace HRManager.Web.Controllers
{
    public class EmployeesController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IEmployeeData _employeeData;

        public EmployeesController(IMapper mapper, IEmployeeData employeeData)
        {
            _mapper = mapper;
            _employeeData = employeeData;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var employees = _employeeData.GetAll().ToList();
            return View(_mapper.Map<IEnumerable<EmployeeViewModel>>(employees));
        }

        [HttpGet]
        public ActionResult Details(int id)
        {
            var employee = _employeeData.Get(id);
            if (employee == null)
            {
                return View("NotFound");
            }

            return View(_mapper.Map<EmployeeViewModel>(employee));
        }

        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.ReportsToList = 
                _mapper.Map<IEnumerable<EmployeeViewModel>>(_employeeData.GetAll())
                .Select(employee => new SelectListItem()
                {
                    Text = employee.FullName, 
                    Value = employee.Id.ToString()
                });
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(EmployeeViewModel model)
        {
            if (ModelState.IsValid)
            {
                var employee = _employeeData.Add(_mapper.Map<Employee>(model));
                return RedirectToAction("Details", new { id = employee.Id });
            }

            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var employee = _employeeData.Get(id);

            if (employee == null)
            {
                return View("NotFound");
            }

            return View(_mapper.Map<EmployeeViewModel>(employee));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EmployeeViewModel model)
        {
            if (ModelState.IsValid)
            {
                _employeeData.Update(_mapper.Map<Employee>(model));
                return RedirectToAction("Details", new { id = model.Id });
            }

            return View(model);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var employee = _employeeData.Get(id);

            if (employee == null)
            {
                return View("NotFound");
            }

            return View(_mapper.Map<EmployeeViewModel>(employee));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, FormCollection _)
        {
            _employeeData.Delete(id);
            return RedirectToAction("Index");
        }
    }
}