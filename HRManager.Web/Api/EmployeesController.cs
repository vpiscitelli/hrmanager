﻿using HRManager.Data.Models;
using HRManager.Data.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebGrease.Css.Extensions;

namespace HRManager.Web.Api
{
    public class EmployeesController : ApiController
    {
        private readonly IEmployeeData _employeeData;

        public EmployeesController(IEmployeeData employeeData)
        {
            _employeeData = employeeData;
        }

        // GET: api/Employees
        public IEnumerable<Employee> Get()
        {
            var model = _employeeData.GetAll();
            model.ForEach(employee => employee.ReportsTo = null);
            return model;
        }

        [Route("api/Employees/GetById/{id}")]
        public Employee GetById(int id)
        {
            var model = _employeeData.Get(id);
            if (model != null)
            {
                model.ReportsTo = null;
            }
            return model;
        }
    }
}
