﻿namespace HRManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveDepartment : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Employees", "Department_Id", "dbo.Departments");
            DropIndex("dbo.Employees", new[] { "Department_Id" });
            AddColumn("dbo.Employees", "DateJoined", c => c.DateTime(nullable: false));
            AddColumn("dbo.Employees", "Position", c => c.String(nullable: false));
            AddColumn("dbo.Employees", "ReportsTo_Id", c => c.Int());
            CreateIndex("dbo.Employees", "ReportsTo_Id");
            AddForeignKey("dbo.Employees", "ReportsTo_Id", "dbo.Employees", "Id");
            DropColumn("dbo.Employees", "JobTitle");
            DropColumn("dbo.Employees", "Department_Id");
            DropTable("dbo.Departments");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Departments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Employees", "Department_Id", c => c.Int());
            AddColumn("dbo.Employees", "JobTitle", c => c.Int(nullable: false));
            DropForeignKey("dbo.Employees", "ReportsTo_Id", "dbo.Employees");
            DropIndex("dbo.Employees", new[] { "ReportsTo_Id" });
            DropColumn("dbo.Employees", "ReportsTo_Id");
            DropColumn("dbo.Employees", "Position");
            DropColumn("dbo.Employees", "DateJoined");
            CreateIndex("dbo.Employees", "Department_Id");
            AddForeignKey("dbo.Employees", "Department_Id", "dbo.Departments", "Id");
        }
    }
}
