﻿namespace HRManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ExpandEmployee : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Employees", "Initial", c => c.String());
            AddColumn("dbo.Employees", "AddressLine1", c => c.String());
            AddColumn("dbo.Employees", "AddressLine2", c => c.String());
            AddColumn("dbo.Employees", "City", c => c.String());
            AddColumn("dbo.Employees", "Province", c => c.String());
            AddColumn("dbo.Employees", "PostalCode", c => c.String());
            AddColumn("dbo.Employees", "HomePhone", c => c.String());
            AddColumn("dbo.Employees", "WorkPhone", c => c.String());
            AddColumn("dbo.Employees", "CellPhone", c => c.String());
            AddColumn("dbo.Employees", "EmailAddress", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Employees", "EmailAddress");
            DropColumn("dbo.Employees", "CellPhone");
            DropColumn("dbo.Employees", "WorkPhone");
            DropColumn("dbo.Employees", "HomePhone");
            DropColumn("dbo.Employees", "PostalCode");
            DropColumn("dbo.Employees", "Province");
            DropColumn("dbo.Employees", "City");
            DropColumn("dbo.Employees", "AddressLine2");
            DropColumn("dbo.Employees", "AddressLine1");
            DropColumn("dbo.Employees", "Initial");
        }
    }
}
