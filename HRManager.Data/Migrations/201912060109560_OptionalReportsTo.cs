﻿namespace HRManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OptionalReportsTo : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Employees", new[] { "ReportsToId" });
            AlterColumn("dbo.Employees", "ReportsToId", c => c.Int());
            CreateIndex("dbo.Employees", "ReportsToId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Employees", new[] { "ReportsToId" });
            AlterColumn("dbo.Employees", "ReportsToId", c => c.Int(nullable: false));
            CreateIndex("dbo.Employees", "ReportsToId");
        }
    }
}
