﻿namespace HRManager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReportsTo : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Employees", new[] { "ReportsTo_Id" });
            RenameColumn(table: "dbo.Employees", name: "ReportsTo_Id", newName: "ReportsToId");
            AlterColumn("dbo.Employees", "ReportsToId", c => c.Int(nullable: false));
            CreateIndex("dbo.Employees", "ReportsToId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Employees", new[] { "ReportsToId" });
            AlterColumn("dbo.Employees", "ReportsToId", c => c.Int());
            RenameColumn(table: "dbo.Employees", name: "ReportsToId", newName: "ReportsTo_Id");
            CreateIndex("dbo.Employees", "ReportsTo_Id");
        }
    }
}
