﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRManager.Data.Models;

namespace HRManager.Data.Services
{
    public class SQLEmployeeData: IEmployeeData
    {
        private readonly HRManagerDbContext _db;

        public SQLEmployeeData(HRManagerDbContext db)
        {
            _db = db;
        }

        public IEnumerable<Employee> GetAll()
        {
            return _db.Employees.OrderBy(e=>e.LastName);
        }

        public Employee Get(int id)
        {
            return _db.Employees.Include(employee => employee.ReportsTo).FirstOrDefault(e => e.Id == id);
        }

        public Employee Add(Employee employee)
        {
            var savedEmployee = _db.Employees.Add(employee);
            _db.SaveChanges();

            return savedEmployee;
        }

        public void Update(Employee employee)
        {
            var entry = _db.Entry(employee);
            entry.State = EntityState.Modified;
            _db.SaveChanges();
        }

        public void Delete(int id)
        {
            var employee = _db.Employees.Find(id);
            if (employee != null)
            {
                _db.Employees.Remove(employee);
                _db.SaveChanges();
            }
        }
    }
}
