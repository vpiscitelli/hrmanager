﻿using System.Collections.Generic;
using System.Text;
using HRManager.Data.Models;

namespace HRManager.Data.Services
{
    public interface IEmployeeData
    {
        IEnumerable<Employee> GetAll();
        Employee Get(int id);
        Employee Add(Employee employee);
        void Update(Employee employee);
        void Delete(int id);
    }
}
