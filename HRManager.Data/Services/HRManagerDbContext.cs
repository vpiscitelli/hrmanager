﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Text;
using HRManager.Data.Models;

namespace HRManager.Data.Services
{
    public class HRManagerDbContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
    }
}
