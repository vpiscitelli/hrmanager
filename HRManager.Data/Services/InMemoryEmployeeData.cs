﻿using System;
using System.Collections.Generic;
using System.Linq;
using HRManager.Data.Models;

namespace HRManager.Data.Services
{
    public class InMemoryEmployeeData : IEmployeeData
    {
        private List<Employee> _employees;

        public InMemoryEmployeeData()
        {
            var president = new Employee
            {
                Id = 1, FirstName = "Vincenzo", LastName = "Piscitelli", Position = "President",
                DateJoined = new DateTime(2019, 12, 01)
            };
            _employees = new List<Employee>()
            {
                president,
                new Employee {Id = 2, FirstName = "John", LastName = "Smith", Position = "Vice President", DateJoined = new DateTime(2019,12,02) , ReportsTo = president},
                new Employee {Id = 3, FirstName = "Jane", LastName = "Doe", Position = "Vice President", DateJoined = new DateTime(2019,12,02), ReportsTo = president},
            };
        }

        public Employee Add(Employee employee)
        {
            employee.Id = _employees.Max(e => e.Id) + 1;
            _employees.Add(employee);

            return employee;
        }

        public Employee Get(int id)
        {
            return _employees.FirstOrDefault(e => e.Id == id);
        }

        public IEnumerable<Employee> GetAll()
        {
            return _employees.OrderBy(e => e.LastName);
        }

        public void Update(Employee employee)
        {
            var existing = Get(employee.Id);

            if(existing != null)
            {
                existing.FirstName = employee.FirstName;
                existing.LastName = employee.LastName;
                existing.Position = employee.Position;
                existing.DateJoined = employee.DateJoined;
                existing.ReportsTo = employee.ReportsTo;
            }
        }

        public void Delete(int id)
        {
            var employee = Get(id);
            if (employee != null)
            {
                _employees.Remove(employee);
            }
        }
    }
}