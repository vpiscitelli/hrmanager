﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace HRManager.Data.Models
{
    public enum JobTitle
    {
        [Display(Name = "President")]
        President,
        [Display(Name = "Vice President")]
        VicePresident,
        [Display(Name = "Manager")]
        Manager,
        [Display(Name = "Employee")]
        Employee
    }
}
