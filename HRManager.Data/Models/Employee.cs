﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Reflection.Emit;
using System.Text;

namespace HRManager.Data.Models
{
    public class Employee
    {
        public int Id { get; set; }

        [Required]
        public string FirstName { get; set; }

        public string Initial { get; set; }

        [Required]
        public string LastName { get; set; }

        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string PostalCode { get; set; }
        public string HomePhone { get; set; }
        public string WorkPhone { get; set; }
        public string CellPhone { get; set; }

        [Required]
        public string EmailAddress { get; set; }

        [Required]
        public DateTime DateJoined { get; set; }

        [Required]
        public string Position { get; set; }

        public int? ReportsToId { get; set; }

        public Employee ReportsTo { get; set; }

    }
}
